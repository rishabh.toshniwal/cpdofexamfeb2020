package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest{

	@Test
	public void testfindmax() throws Exception {
		int res = new MinMax().findmax(3,4);
		assertEquals("findmax", 4, res);
	}

@Test
public void testbar() throws Exception {
		String  res = new MinMax().bar("xx");
		assertEquals("test", "xx", res);
	}
} 
