package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutCPDOFTest{

	@Test
	public void testDesc() throws Exception {
		String res = new AboutCPDOF().desc();

		assertTrue(res.contains("CP-DOF"));
	}
} 
